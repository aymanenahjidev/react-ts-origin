declare namespace API {
    type Quiz = {
        id:number;
        title:string;
        description:string;
    }
}