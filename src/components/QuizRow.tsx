import React from "react";

type Props = {
  quiz:API.Quiz,
};

const QuizRow = (props: Props) => (
  <>
    <span className="font-bold underline border-b">{props.quiz.title}</span>
    <span className="text-gray-500 border-b">{props.quiz.description}</span>
  </>
);

export default QuizRow;
